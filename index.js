const log4js = require("log4js");

let logger = log4js.getLogger();

logger.level = "debug";

logger.info("La aplicacion se ejecuto con exito");

function sumar(x,y){
  return x + y;
}

module.exports = sumar;
